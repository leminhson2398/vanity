package service

import (
	"fmt"
	"strings"

	"go.jolheiser.com/vanity/api"
	"go.jolheiser.com/vanity/flags"
)

type Service interface {
	Packages() (map[string]*api.Package, error)
	GoDir(*api.Package) string
	GoFile(*api.Package) string
	GoMod(*api.Package) (string, error)
}

func New() Service {
	switch strings.ToLower(flags.Service) {
	case "gitea":
		return NewGitea()
	case "github":
		return NewGitHub()
	case "gitlab":
		return NewGitLab()
	default:
		return Off{}
	}
}

func Check(pkg *api.Package) error {

	// Private
	if pkg.Private && !flags.Private {
		return fmt.Errorf("%s is private and --private wasn't used", pkg.Name)
	}

	// Forked
	if pkg.Fork && !flags.Fork {
		return fmt.Errorf("%s is a fork and --fork wasn't used", pkg.Name)
	}

	// Mirrored
	if pkg.Mirror && !flags.Mirror {
		return fmt.Errorf("%s is a mirror and --mirror wasn't used", pkg.Name)
	}

	// Archived
	if pkg.Archive && !flags.Archive {
		return fmt.Errorf("%s is archived and --archive wasn't used", pkg.Name)
	}

	// Exclusions
	for _, exclude := range flags.Exclude {
		if exclude.MatchString(pkg.Name) {
			return fmt.Errorf("%s was excluded by the rule %s", pkg.Name, exclude.String())
		}
	}

	// Inclusions
	if len(flags.Include) > 0 {
		var included bool
		for _, include := range flags.Include {
			if include.MatchString(pkg.Name) {
				included = true
				break
			}
		}
		if !included {
			return fmt.Errorf("%s wasn't included by any existing inclusion rule", pkg.Name)
		}
	}

	return nil
}
