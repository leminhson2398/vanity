package service

import "go.jolheiser.com/vanity/api"

var _ Service = Off{}

type Off struct{}

func (o Off) Packages() (map[string]*api.Package, error) {
	return make(map[string]*api.Package), nil
}

func (o Off) GoDir(*api.Package) string {
	return ""
}

func (o Off) GoFile(*api.Package) string {
	return ""
}

func (o Off) GoMod(*api.Package) (string, error) {
	return "", nil
}
