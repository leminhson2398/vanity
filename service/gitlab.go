package service

import (
	"fmt"
	"html"

	"go.jolheiser.com/vanity/api"
	"go.jolheiser.com/vanity/flags"

	"github.com/xanzy/go-gitlab"
	"go.jolheiser.com/beaver"
)

var _ Service = &GitLab{}

func NewGitLab() *GitLab {
	client, err := gitlab.NewClient(flags.Token, gitlab.WithBaseURL(flags.BaseURL.String()))
	if err != nil {
		beaver.Errorf("could not create GitLab client: %v", err)
	}
	return &GitLab{
		client: client,
	}
}

type GitLab struct {
	client *gitlab.Client
}

func (g GitLab) Packages() (map[string]*api.Package, error) {
	packages := make(map[string]*api.Package)
	page := 0
	for {
		opts := gitlab.ListProjectsOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 50,
			},
		}

		repos, _, err := g.client.Projects.ListUserProjects(flags.Namespace, &opts)
		if err != nil {
			return nil, err
		}

		for _, repo := range repos {
			packages[repo.Name] = &api.Package{
				Name:        repo.Name,
				Description: repo.Description,
				Branch:      repo.DefaultBranch,
				WebURL:      repo.WebURL,
				CloneHTTP:   repo.HTTPURLToRepo,
				CloneSSH:    repo.SSHURLToRepo,
				Private:     repo.Visibility != gitlab.PublicVisibility,
				Fork:        repo.ForkedFromProject != nil,
				Mirror:      repo.Mirror,
				Archive:     repo.Archived,
				Topics:      repo.TagList,
			}
		}

		page++
		if len(repos) == 0 {
			break
		}
	}

	return packages, nil
}

func (g GitLab) GoDir(pkg *api.Package) string {
	return fmt.Sprintf("%s/-/tree/%s{/dir}", pkg.WebURL, pkg.Branch)
}

func (g GitLab) GoFile(pkg *api.Package) string {
	return fmt.Sprintf("%s/-/blob/%s{/dir}/{file}#L{line}", pkg.WebURL, pkg.Branch)
}

func (g GitLab) GoMod(pkg *api.Package) (string, error) {
	id := fmt.Sprintf("%s/%s", flags.Namespace, pkg.Name)
	content, _, err := g.client.RepositoryFiles.GetRawFile(html.EscapeString(id), "go.mod", &gitlab.GetRawFileOptions{
		Ref: &pkg.Branch,
	})
	return string(content), err
}
