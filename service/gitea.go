package service

import (
	"fmt"

	"go.jolheiser.com/vanity/api"
	"go.jolheiser.com/vanity/flags"

	"code.gitea.io/sdk/gitea"
	"go.jolheiser.com/beaver"
)

var _ Service = &Gitea{}

func NewGitea() *Gitea {
	client, err := gitea.NewClient(flags.BaseURL.String(), gitea.SetToken(flags.Token))
	if err != nil {
		beaver.Errorf("could not create Gitea client: %v", err)
	}
	return &Gitea{
		client: client,
	}
}

type Gitea struct {
	client *gitea.Client
}

func (g Gitea) Packages() (map[string]*api.Package, error) {
	packages := make(map[string]*api.Package)
	topicOpts := gitea.ListRepoTopicsOptions{
		ListOptions: gitea.ListOptions{
			Page:     1,
			PageSize: 50,
		},
	}
	page := 0
	for {
		opts := gitea.ListReposOptions{
			ListOptions: gitea.ListOptions{
				Page:     page,
				PageSize: 50,
			},
		}

		repos, _, err := g.client.ListUserRepos(flags.Namespace, opts)
		if err != nil {
			return nil, err
		}

		for _, repo := range repos {
			pkg := &api.Package{
				Name:        repo.Name,
				Description: repo.Description,
				Branch:      repo.DefaultBranch,
				WebURL:      repo.HTMLURL,
				CloneHTTP:   repo.CloneURL,
				CloneSSH:    repo.SSHURL,
				Private:     repo.Private,
				Fork:        repo.Fork,
				Mirror:      repo.Mirror,
				Archive:     repo.Archived,
			}

			// Get tags
			topics, _, err := g.client.ListRepoTopics(flags.Namespace, repo.Name, topicOpts)
			if err != nil {
				beaver.Warnf("could not get topics for %s: %v", repo.Name, err)
			}
			pkg.Topics = topics

			packages[repo.Name] = pkg
		}

		page++
		if len(repos) == 0 {
			break
		}
	}

	return packages, nil
}

func (g Gitea) GoDir(pkg *api.Package) string {
	return fmt.Sprintf("%s/src/branch/%s{/dir}", pkg.WebURL, pkg.Branch)
}

func (g Gitea) GoFile(pkg *api.Package) string {
	return fmt.Sprintf("%s/src/branch/%s{/dir}/{file}#L{line}", pkg.WebURL, pkg.Branch)
}

func (g Gitea) GoMod(pkg *api.Package) (string, error) {
	content, _, err := g.client.GetFile(flags.Namespace, pkg.Name, pkg.Branch, "go.mod")
	return string(content), err
}
