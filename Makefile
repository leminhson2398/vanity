GO ?= go

VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')

.PHONY: build
build:
	$(GO) build -ldflags '-s -w -X "go.jolheiser.com/vanity/api.Version=$(VERSION)"'

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test --race ./...

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: docker-build
docker-build:
	docker build -f docker/Dockerfile -t jolheiser/vanity .

.PHONY: docker-push
docker-push:
	docker push jolheiser/vanity