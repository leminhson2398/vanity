package main

import (
	"fmt"
	"net/http"
	"os"

	"go.jolheiser.com/vanity/api"
	"go.jolheiser.com/vanity/flags"
	"go.jolheiser.com/vanity/router"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

func main() {
	app := cli.NewApp()
	app.Name = "vanity"
	app.Usage = "Vanity Go Imports"
	app.Version = api.Version
	app.Action = doAction
	app.Flags = flags.Flags
	app.Before = flags.Before

	beaver.Console.Format = beaver.FormatOptions{
		TimePrefix:  true,
		StackPrefix: true,
		StackLimit:  15,
		LevelPrefix: true,
		LevelColor:  true,
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}

func doAction(ctx *cli.Context) error {
	mux, err := router.Init()
	if err != nil {
		return err
	}
	if err := http.ListenAndServe(fmt.Sprintf(":%s", ctx.String("port")), mux); err != nil {
		return err
	}
	return nil
}
