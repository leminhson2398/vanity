module go.jolheiser.com/vanity

go 1.16

require (
	code.gitea.io/sdk/gitea v0.13.2
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1 // indirect
	github.com/google/go-github/v32 v32.1.0
	github.com/pelletier/go-toml v1.8.1
	github.com/urfave/cli/v2 v2.2.0
	github.com/xanzy/go-gitlab v0.37.0
	go.jolheiser.com/beaver v1.0.2
	go.jolheiser.com/overlay v0.0.2 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
)
