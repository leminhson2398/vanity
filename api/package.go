package api

import (
	"fmt"
	"strings"
)

type Package struct {
	Name        string   `toml:"name"`
	Description string   `toml:"description"`
	Branch      string   `toml:"branch"`
	WebURL      string   `toml:"web_url"`
	CloneHTTP   string   `toml:"clone_http"`
	CloneSSH    string   `toml:"clone_ssh"`
	Topics      []string `toml:"topics"`

	Private bool `toml:"-"`
	Fork    bool `toml:"-"`
	Mirror  bool `toml:"-"`
	Archive bool `toml:"-"`
}

func (p *Package) Module(domain string) string {
	return fmt.Sprintf("%s/%s", domain, strings.ToLower(p.Name))
}
