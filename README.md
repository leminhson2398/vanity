# Vanity

A simple web service to serve [vanity Go imports](https://golang.org/cmd/go/#hdr-Remote_import_paths). Feel free to check it out using [my instance](https://go.jolheiser.com/).

Vanity also supports [git-import](https://gitea.com/jolheiser/git-import).

## Configuration

When choosing a service, the default `base-url` will be the default server of that service:

| Service | Default            |
|:-------:|:------------------:|
| Gitea   | https://gitea.com  |
| GitHub  | https://github.com |
| GitLab  | https://gitlab.com |

```
NAME:
   vanity - Vanity Go Imports

USAGE:
   vanity [global options] command [command options] [arguments...]

VERSION:
   0.1.0+3-g6d7150e

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value     Path to a config file [$VANITY_CONFIG]
   --port value       Port to run the vanity server on (default: 7777) [$VANITY_PORT]
   --domain value     Vanity domain, e.g. go.domain.tld [$VANITY_DOMAIN]
   --service value    Service type (Gitea, GitHub, GitLab) (default: "gitea") [$VANITY_SERVICE]
   --base-url value   Base URL to service [$VANITY_BASE_URL]
   --namespace value  Owner namespace [$VANITY_NAMESPACE]
   --token value      Access token [$VANITY_TOKEN]
   --include value    Repository names to include (regex) [$VANITY_INCLUDE]
   --exclude value    Repository names to exclude (regex) [$VANITY_EXCLUDE]
   --private          Include private repositories (default: false) [$VANITY_PRIVATE]
   --fork             Include forked repositories (default: false) [$VANITY_FORK]
   --mirror           Include mirrored repositories (default: false) [$VANITY_MIRROR]
   --archive          Include archived repositories (default: false) [$VANITY_ARCHIVE]
   --override value   Repository name to override (NAME=OVERRIDE) [$VANITY_OVERRIDE]
   --interval value   Interval between updating repositories (default: 15m0s) [$VANITY_INTERVAL]
   --debug            Debug logging (default: false) [$VANITY_DEBUG]
   --help, -h         show help (default: false)
   --version, -v      print the version (default: false)
```

## Docker

```sh
docker run \
    --env VANITY_DOMAIN=go.domain.tld \
    --env VANITY_NAMESPACE=<jolheiser> \
    --env VANITY_TOKEN=<token> \
    --publish 80:7777 \
    --restart always
    jolheiser/vanity:latest
```

## Overrides

Certain modules may not align perfectly with their repository name.

Overrides are available via config or by setting an environment variable `VANITY_OVERRIDE_PACKAGE=NAME`

## Config-only Mode

To run Vanity in config-only mode for packages, set `--service` to `off`.

## Manual Mode

To run Vanity without automatic updating, use `--manual`.

When running with manual-mode, the provided button or `/_/update` endpoint can be used once every `--interval`.

## Topic Lists

By setting `--topics`, anyone visiting the index page will see packages grouped by their topics.

Regardless of the setting, you can switch beteween list-view and topic-view with the provided button
or changing the URL between `?format=list` and `?format=topics`.

## API

In order to preserve namespaces for packages, Vanity's API uses the URL `/_/{endpoint}`

Vanity currently supports `/_/status` and `/_/update`, to get some status information and update the package cache respectively.

## License

[MIT](LICENSE)