package router

import (
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/vanity/service"
	"time"

	"go.jolheiser.com/vanity/flags"
)

var (
	svc        service.Service
	lastUpdate time.Time
	canUpdate  bool
)

func cronStart() {
	canUpdate = true
	ticker := time.NewTicker(flags.Interval)
	for {
		<-ticker.C
		if !flags.Manual && canUpdate {
			beaver.Debug("Running package update...")
			updateCache()
			beaver.Debugf("Finished package update: %s", cache.Packages.Names())
			lastUpdate = time.Now()
		}
		canUpdate = true
	}
}
