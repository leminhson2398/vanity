package router

import (
	"embed"
	"go.jolheiser.com/overlay"
	"go.jolheiser.com/vanity/api"
	"html/template"
	"os"
	"path/filepath"
	"runtime"
)

//go:embed templates
var templates embed.FS

func Templates() (*template.Template, error) {
	bin, err := os.Executable()
	if err != nil {
		return nil, err
	}
	customPath := os.Getenv("VANITY_CUSTOM")
	if customPath == "" {
		customPath = filepath.Join(bin, "custom")
	}

	ofs, err := overlay.New(customPath, templates)
	if err != nil {
		return nil, err
	}

	return template.New("vanity").Funcs(funcMap).ParseFS(ofs, "templates/*")
}

var funcMap = template.FuncMap{
	"AppVer": func() string {
		return api.Version
	},
	"GoVer": func() string {
		return runtime.Version()
	},
	"CanUpdate": func() bool {
		return canUpdate
	},
}
