package flags

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"go.jolheiser.com/vanity/api"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var (
	configPath string
	baseURL    string
	include    cli.StringSlice
	exclude    cli.StringSlice
	override   cli.StringSlice

	Port      int
	Domain    string
	Service   string
	BaseURL   *url.URL
	Namespace string
	Token     string
	Include   []*regexp.Regexp
	Exclude   []*regexp.Regexp
	Private   bool
	Fork      bool
	Mirror    bool
	Archive   bool
	Override  = make(map[string]string)
	Interval  time.Duration
	Manual    bool
	Topics    bool
	Debug     bool

	ConfigPackages []*api.Package
)

var Flags = []cli.Flag{
	&cli.StringFlag{
		Name:        "config",
		Usage:       "Path to a config file",
		EnvVars:     []string{"VANITY_CONFIG"},
		Destination: &configPath,
	},
	&cli.IntFlag{
		Name:        "port",
		Usage:       "Port to run the vanity server on",
		Value:       7777,
		EnvVars:     []string{"VANITY_PORT"},
		Destination: &Port,
	},
	&cli.StringFlag{
		Name:        "domain",
		Usage:       "Vanity domain, e.g. go.domain.tld",
		EnvVars:     []string{"VANITY_DOMAIN"},
		Required:    true,
		Destination: &Domain,
	},
	&cli.StringFlag{
		Name:        "service",
		Usage:       "Service type (Gitea, GitHub, GitLab)",
		Value:       "gitea",
		EnvVars:     []string{"VANITY_SERVICE"},
		Destination: &Service,
	},
	&cli.StringFlag{
		Name:        "base-url",
		Usage:       "Base URL to service",
		EnvVars:     []string{"VANITY_BASE_URL"},
		Destination: &baseURL,
	},
	&cli.StringFlag{
		Name:        "namespace",
		Usage:       "Owner namespace",
		EnvVars:     []string{"VANITY_NAMESPACE"},
		Destination: &Namespace,
	},
	&cli.StringFlag{
		Name:        "token",
		Usage:       "Access token",
		EnvVars:     []string{"VANITY_TOKEN"},
		Destination: &Token,
	},
	&cli.StringSliceFlag{
		Name:        "include",
		Usage:       "Repository names to include (regex)",
		EnvVars:     []string{"VANITY_INCLUDE"},
		Destination: &include,
	},
	&cli.StringSliceFlag{
		Name:        "exclude",
		Usage:       "Repository names to exclude (regex)",
		EnvVars:     []string{"VANITY_EXCLUDE"},
		Destination: &exclude,
	},
	&cli.BoolFlag{
		Name:        "private",
		Usage:       "Include private repositories",
		EnvVars:     []string{"VANITY_PRIVATE"},
		Destination: &Private,
	},
	&cli.BoolFlag{
		Name:        "fork",
		Usage:       "Include forked repositories",
		EnvVars:     []string{"VANITY_FORK"},
		Destination: &Fork,
	},
	&cli.BoolFlag{
		Name:        "mirror",
		Usage:       "Include mirrored repositories",
		EnvVars:     []string{"VANITY_MIRROR"},
		Destination: &Mirror,
	},
	&cli.BoolFlag{
		Name:        "archive",
		Usage:       "Include archived repositories",
		EnvVars:     []string{"VANITY_ARCHIVE"},
		Destination: &Archive,
	},
	&cli.StringSliceFlag{
		Name:        "override",
		Usage:       "Repository name to override (NAME=OVERRIDE)",
		EnvVars:     []string{"VANITY_OVERRIDE"},
		Destination: &override,
	},
	&cli.DurationFlag{
		Name:        "interval",
		Usage:       "Interval between updating repositories",
		Value:       time.Minute * 15,
		EnvVars:     []string{"VANITY_INTERVAL"},
		Destination: &Interval,
	},
	&cli.BoolFlag{
		Name:        "manual",
		Usage:       "Disable cron and only update with endpoint",
		EnvVars:     []string{"VANITY_MANUAL"},
		Destination: &Manual,
	},
	&cli.BoolFlag{
		Name:        "topics",
		Usage:       "Group projects by topic by default",
		EnvVars:     []string{"VANITY_TOPICS"},
		Destination: &Topics,
	},
	&cli.BoolFlag{
		Name:        "debug",
		Usage:       "Debug logging",
		EnvVars:     []string{"VANITY_DEBUG"},
		Destination: &Debug,
	},
}

func Before(ctx *cli.Context) error {
	setConfig(ctx)

	var defaultURL string
	var configOnly bool
	switch strings.ToLower(Service) {
	case "gitea":
		defaultURL = "https://gitea.com"
	case "github":
		defaultURL = "https://github.com"
	case "gitlab":
		defaultURL = "https://gitlab.com"
	case "off":
		configOnly = true
		beaver.Infof("Running in config-only mode")
		defaultURL = "https://domain.tld"
	default:
		return errors.New("unrecognized service type")
	}

	if baseURL == "" {
		baseURL = defaultURL
	}

	var err error
	BaseURL, err = url.Parse(baseURL)
	if err != nil {
		return err
	}

	if !configOnly {
		errs := make([]string, 0, 2)
		if Namespace == "" {
			errs = append(errs, "namespace")
		}
		if Token == "" {
			errs = append(errs, "token")
		}
		if len(errs) > 0 {
			return fmt.Errorf("%s is required with a service", strings.Join(errs, ", "))
		}
	}

	Include = make([]*regexp.Regexp, len(include.Value()))
	for idx, i := range include.Value() {
		Include[idx] = regexp.MustCompile(i)
	}

	Exclude = make([]*regexp.Regexp, len(exclude.Value()))
	for idx, e := range exclude.Value() {
		Exclude[idx] = regexp.MustCompile(e)
	}

	if Manual {
		beaver.Info("Running in manual mode")
	}

	if Debug {
		beaver.Console.Level = beaver.DEBUG
	}

	beaver.Debugf("Port: %d", Port)
	beaver.Debugf("Domain: %s", Domain)
	beaver.Debugf("Service: %s", Service)
	beaver.Debugf("Base URL: %s", baseURL)
	beaver.Debugf("Namespace: %s", Namespace)
	beaver.Debugf("Include: %s", include.Value())
	beaver.Debugf("Exclude: %s", exclude.Value())
	beaver.Debugf("Private: %t", Private)
	beaver.Debugf("Fork: %t", Fork)
	beaver.Debugf("Mirror: %t", Mirror)
	beaver.Debugf("Archive: %t", Archive)
	beaver.Debugf("Override: %s", override.Value())
	beaver.Debugf("Interval: %s", Interval)
	beaver.Debugf("Manual: %t", Manual)
	return nil
}
